# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171023221821) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "intarray"
  enable_extension "pg_stat_statements"

  create_table "accounts", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "authentication_token",   limit: 255
    t.integer  "user_id",                                         null: false
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.integer  "invitation_id"
    t.string   "invitation_type",        limit: 255
  end

  add_index "accounts", ["authentication_token"], name: "index_accounts_on_authentication_token", unique: true, using: :btree
  add_index "accounts", ["email"], name: "index_accounts_on_email", unique: true, using: :btree
  add_index "accounts", ["invitation_id"], name: "index_accounts_on_user_invitation_id", using: :btree
  add_index "accounts", ["reset_password_token"], name: "index_accounts_on_reset_password_token", unique: true, using: :btree
  add_index "accounts", ["user_id"], name: "index_accounts_on_user_id", unique: true, using: :btree

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type",      limit: 255
    t.integer  "owner_id"
    t.string   "owner_type",          limit: 255
    t.string   "key",                 limit: 255
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type",      limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.boolean  "visible",                         default: false
    t.datetime "published_at"
    t.string   "membershipable_type", limit: 255
    t.integer  "membershipable_id"
  end

  add_index "activities", ["membershipable_id", "membershipable_type"], name: "index_activities_on_membershipable_id_and_membershipable_type", using: :btree
  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree
  add_index "activities", ["trackable_type"], name: "index_activities_on_trackable_type", using: :btree

  create_table "addresses", force: :cascade do |t|
    t.integer  "addressable_id"
    t.string   "street1",          limit: 255
    t.string   "street2",          limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "addressable_type", limit: 255
    t.integer  "zip_code_id"
    t.boolean  "primary"
    t.float    "latitude"
    t.float    "longitude"
  end

  add_index "addresses", ["addressable_id", "addressable_type"], name: "index_addresses_on_addressable_id_and_addressable_type", using: :btree
  add_index "addresses", ["zip_code_id"], name: "index_addresses_on_zip_code_id", using: :btree

  create_table "api_keys", force: :cascade do |t|
    t.string   "access_token", limit: 255
    t.string   "name",         limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "owner_id"
    t.string   "owner_type",   limit: 255
  end

  create_table "apprenticeship_occupation_requirements", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "work_process_id"
    t.decimal  "hours"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "role_id"
  end

  add_index "apprenticeship_occupation_requirements", ["role_id", "work_process_id"], name: "by_role_and_work_process", unique: true, using: :btree
  add_index "apprenticeship_occupation_requirements", ["user_id"], name: "index_apprenticeship_occupation_requirements_on_user_id", using: :btree
  add_index "apprenticeship_occupation_requirements", ["work_process_id"], name: "index_apprenticeship_occupation_requirements_on_work_process_id", using: :btree

  create_table "apprenticeship_occupations", force: :cascade do |t|
    t.integer  "program_id"
    t.integer  "organization_id"
    t.string   "name",                       limit: 255
    t.text     "description"
    t.integer  "onet_code_id"
    t.datetime "valid_from"
    t.datetime "valid_to"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "duration_in_months"
    t.string   "skill_review_frequency",     limit: 255
    t.string   "general_review_frequency",   limit: 255
    t.string   "classroom_review_frequency", limit: 255
    t.string   "type",                       limit: 255
  end

  add_index "apprenticeship_occupations", ["id", "type"], name: "index_apprenticeship_occupations_on_id_and_type", using: :btree
  add_index "apprenticeship_occupations", ["onet_code_id"], name: "index_apprenticeship_occupations_on_onet_code_id", using: :btree
  add_index "apprenticeship_occupations", ["organization_id"], name: "index_apprenticeship_occupations_on_organization_id", using: :btree
  add_index "apprenticeship_occupations", ["program_id"], name: "index_apprenticeship_occupations_on_program_id", using: :btree

  create_table "apprenticeship_onet_codes", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "apprenticeship_programs", force: :cascade do |t|
    t.integer  "organization_id"
    t.string   "name",            limit: 255
    t.text     "description"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.datetime "legacy_at"
  end

  add_index "apprenticeship_programs", ["organization_id"], name: "index_apprenticeship_programs_on_organization_id", using: :btree

  create_table "apprenticeship_reports", force: :cascade do |t|
    t.integer  "program_id"
    t.integer  "role_id"
    t.string   "type",              limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  add_index "apprenticeship_reports", ["id", "type"], name: "index_apprenticeship_reports_on_id_and_type", using: :btree
  add_index "apprenticeship_reports", ["program_id"], name: "index_apprenticeship_reports_on_program_id", using: :btree
  add_index "apprenticeship_reports", ["role_id"], name: "index_apprenticeship_reports_on_role_id", using: :btree

  create_table "apprenticeship_review_items", force: :cascade do |t|
    t.integer  "review_id"
    t.integer  "skill_id"
    t.integer  "apprentice_role_id"
    t.string   "response_type",             limit: 255
    t.string   "response",                  limit: 255
    t.text     "note"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "work_process_id"
    t.integer  "occupation_requirement_id"
    t.datetime "completed_at"
  end

  add_index "apprenticeship_review_items", ["apprentice_role_id"], name: "index_apprenticeship_review_items_on_apprentice_role_id", using: :btree
  add_index "apprenticeship_review_items", ["review_id"], name: "index_apprenticeship_review_items_on_review_id", using: :btree
  add_index "apprenticeship_review_items", ["skill_id"], name: "index_apprenticeship_review_items_on_skill_id", using: :btree
  add_index "apprenticeship_review_items", ["work_process_id"], name: "index_apprenticeship_review_items_on_work_process_id", using: :btree

  create_table "apprenticeship_reviewer_connections", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "reviewer_id"
    t.integer  "program_id"
    t.integer  "organization_id"
    t.datetime "valid_from"
    t.datetime "valid_to"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "reviewer_role",    limit: 255
    t.boolean  "primary",                      default: false
    t.integer  "user_role_id"
    t.integer  "reviewer_role_id"
  end

  add_index "apprenticeship_reviewer_connections", ["organization_id"], name: "index_apprenticeship_reviewer_connections_on_organization_id", using: :btree
  add_index "apprenticeship_reviewer_connections", ["program_id"], name: "index_apprenticeship_reviewer_connections_on_program_id", using: :btree
  add_index "apprenticeship_reviewer_connections", ["user_role_id", "reviewer_role_id"], name: "apprenticeship_reviewer_connections_roles_unique", unique: true, using: :btree

  create_table "apprenticeship_reviews", force: :cascade do |t|
    t.integer  "reviewer_role_id"
    t.integer  "apprentice_role_id"
    t.integer  "program_id"
    t.string   "type",               limit: 255
    t.datetime "submitted_at"
    t.datetime "reviewed_at"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.text     "notes"
  end

  add_index "apprenticeship_reviews", ["apprentice_role_id"], name: "index_apprenticeship_reviews_on_apprentice_role_id", using: :btree
  add_index "apprenticeship_reviews", ["id", "type"], name: "index_apprenticeship_reviews_on_id_and_type", using: :btree
  add_index "apprenticeship_reviews", ["program_id"], name: "index_apprenticeship_reviews_on_program_id", using: :btree
  add_index "apprenticeship_reviews", ["reviewer_role_id"], name: "index_apprenticeship_reviews_on_reviewer_role_id", using: :btree

  create_table "apprenticeship_roles", force: :cascade do |t|
    t.integer  "membership_id"
    t.string   "type",          limit: 255
    t.integer  "created_by_id"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.datetime "activated_at"
    t.integer  "employer_id"
  end

  add_index "apprenticeship_roles", ["created_by_id"], name: "index_apprenticeship_roles_on_created_by_id", using: :btree
  add_index "apprenticeship_roles", ["employer_id"], name: "index_apprenticeship_roles_on_employer_id", using: :btree
  add_index "apprenticeship_roles", ["id", "type"], name: "index_apprenticeship_roles_on_id_and_type", using: :btree
  add_index "apprenticeship_roles", ["membership_id"], name: "index_apprenticeship_roles_on_membership_id", using: :btree

  create_table "apprenticeship_skills", force: :cascade do |t|
    t.integer  "occupation_id",               null: false
    t.integer  "work_process_id"
    t.integer  "external_id"
    t.string   "name",            limit: 255
    t.text     "description"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "apprenticeship_skills", ["occupation_id"], name: "index_apprenticeship_skills_on_occupation_id", using: :btree
  add_index "apprenticeship_skills", ["work_process_id"], name: "index_apprenticeship_skills_on_work_process_id", using: :btree

  create_table "apprenticeship_statuses", force: :cascade do |t|
    t.integer  "statusable_id"
    t.string   "statusable_type", limit: 255
    t.string   "state",           limit: 255
    t.integer  "updated_by_id"
    t.text     "notes"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "type",            limit: 255
    t.text     "snapshot"
  end

  add_index "apprenticeship_statuses", ["id", "type"], name: "index_apprenticeship_statuses_on_id_and_type", using: :btree
  add_index "apprenticeship_statuses", ["statusable_type", "statusable_id"], name: "apprenticeship_statusable", using: :btree
  add_index "apprenticeship_statuses", ["updated_by_id"], name: "index_apprenticeship_statuses_on_updated_by_id", using: :btree

  create_table "apprenticeship_time_cards", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "reviewer_id"
    t.integer  "user_role_id"
    t.integer  "reviewer_role_id"
    t.integer  "created_by_id"
  end

  add_index "apprenticeship_time_cards", ["created_by_id"], name: "index_apprenticeship_time_cards_on_created_by_id", using: :btree
  add_index "apprenticeship_time_cards", ["ended_at"], name: "index_apprenticeship_time_cards_on_ended_at", using: :btree
  add_index "apprenticeship_time_cards", ["reviewer_id"], name: "index_apprenticeship_time_cards_on_reviewer_id", using: :btree
  add_index "apprenticeship_time_cards", ["reviewer_role_id"], name: "index_apprenticeship_time_cards_on_reviewer_role_id", using: :btree
  add_index "apprenticeship_time_cards", ["user_id"], name: "index_apprenticeship_time_cards_on_user_id", using: :btree
  add_index "apprenticeship_time_cards", ["user_role_id"], name: "index_apprenticeship_time_cards_on_user_role_id", using: :btree

  create_table "apprenticeship_time_entries", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "time_card_id"
    t.integer  "work_process_id"
    t.decimal  "hours",                     default: 0.0
    t.date     "worked_on"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "role_id"
    t.integer  "occupation_requirement_id"
  end

  add_index "apprenticeship_time_entries", ["occupation_requirement_id"], name: "index_apprenticeship_time_entries_on_occupation_requirement_id", using: :btree
  add_index "apprenticeship_time_entries", ["role_id"], name: "index_apprenticeship_time_entries_on_role_id", using: :btree
  add_index "apprenticeship_time_entries", ["time_card_id"], name: "index_apprenticeship_time_entries_on_time_card_id", using: :btree
  add_index "apprenticeship_time_entries", ["user_id"], name: "index_apprenticeship_time_entries_on_user_id", using: :btree
  add_index "apprenticeship_time_entries", ["work_process_id"], name: "index_apprenticeship_time_entries_on_work_process_id", using: :btree

  create_table "apprenticeship_work_processes", force: :cascade do |t|
    t.integer  "occupation_id"
    t.string   "name",          limit: 255
    t.text     "description"
    t.integer  "default_hours",             default: 0
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "external_id",   limit: 255
  end

  add_index "apprenticeship_work_processes", ["occupation_id"], name: "index_apprenticeship_work_processes_on_occupation_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name",                                 limit: 255
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.string   "avatar_file_name",                     limit: 255
    t.string   "avatar_content_type",                  limit: 255
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "slug",                                 limit: 255
    t.string   "icon",                                 limit: 255
    t.integer  "memberships_count",                                default: 0, null: false
    t.integer  "non_gray_non_admin_memberships_count",             default: 0, null: false
    t.integer  "non_gray_admin_memberships_count",                 default: 0, null: false
    t.integer  "non_gray_total_memberships_count",                 default: 0, null: false
  end

  add_index "categories", ["slug"], name: "index_categories_on_slug", using: :btree

  create_table "categorizations", force: :cascade do |t|
    t.integer  "category_id"
    t.integer  "categorizable_id"
    t.string   "categorizable_type", limit: 255
    t.integer  "created_by_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "user_category_id"
  end

  create_table "cities", force: :cascade do |t|
    t.integer  "state_id"
    t.string   "name",          limit: 255
    t.string   "slug",          limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "unscoped_slug", limit: 255
  end

  add_index "cities", ["unscoped_slug"], name: "index_cities_on_unscoped_slug", using: :btree

  create_table "connection_requests", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.datetime "accepted_at"
    t.datetime "rejected_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "connection_requests", ["recipient_id"], name: "index_connection_requests_on_recipient_id", using: :btree
  add_index "connection_requests", ["sender_id"], name: "index_connection_requests_on_sender_id", using: :btree

  create_table "craigslist_sites", force: :cascade do |t|
    t.integer  "value"
    t.string   "abbreviation", limit: 255
    t.boolean  "main_area"
    t.string   "description",  limit: 255
    t.string   "display_name", limit: 255
    t.string   "state",        limit: 255
    t.string   "subdomain",    limit: 255
    t.string   "country",      limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "zip_code_id"
  end

  create_table "daily_signup_summaries", force: :cascade do |t|
    t.date     "day"
    t.date     "week"
    t.integer  "total_users"
    t.integer  "total_signups"
    t.integer  "total_android"
    t.integer  "total_iphone"
    t.integer  "total_facebook"
    t.text     "invite_code_counts"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "weekly_signup_summary_id"
  end

  add_index "daily_signup_summaries", ["week", "day"], name: "index_daily_signup_summaries_on_week_and_day", using: :btree

  create_table "departments", force: :cascade do |t|
    t.integer  "organization_id"
    t.string   "name",                                 limit: 255
    t.integer  "created_by_id"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.string   "slug",                                 limit: 255
    t.text     "description"
    t.string   "url",                                  limit: 255
    t.string   "phone",                                limit: 255
    t.string   "email",                                limit: 255
    t.integer  "industry_id"
    t.integer  "memberships_count",                                default: 0, null: false
    t.integer  "non_gray_non_admin_memberships_count",             default: 0, null: false
    t.integer  "non_gray_admin_memberships_count",                 default: 0, null: false
    t.integer  "non_gray_total_memberships_count",                 default: 0, null: false
  end

  add_index "departments", ["organization_id"], name: "index_departments_on_organization_id", using: :btree
  add_index "departments", ["slug"], name: "index_departments_on_slug", using: :btree

  create_table "devices", force: :cascade do |t|
    t.string   "device_type", limit: 255
    t.integer  "user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "api_key_id"
    t.string   "device_uid",  limit: 255
    t.string   "token",       limit: 255
  end

  create_table "educations", force: :cascade do |t|
    t.integer  "school_id"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "user_id"
    t.text     "description"
    t.string   "department",    limit: 255
    t.string   "degree",        limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "department_id"
    t.integer  "title_id"
    t.datetime "migrated_at"
    t.integer  "new_id"
    t.string   "new_type",      limit: 255
  end

  add_index "educations", ["department_id"], name: "index_educations_on_department_id", using: :btree
  add_index "educations", ["school_id"], name: "index_educations_on_school_id", using: :btree
  add_index "educations", ["user_id"], name: "index_educations_on_user_id", using: :btree

  create_table "email_messages", force: :cascade do |t|
    t.integer  "from_user_id"
    t.integer  "to_user_id"
    t.string   "from_email",      limit: 255
    t.string   "to_email",        limit: 255
    t.text     "content"
    t.string   "subject",         limit: 255
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "user_id"
    t.integer  "conversation_id"
    t.boolean  "unread",                      default: true
  end

  add_index "email_messages", ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree
  add_index "email_messages", ["from_user_id", "to_user_id"], name: "index_messages_on_from_user_id_and_to_user_id", using: :btree
  add_index "email_messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "endorsement_requests", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "email",            limit: 255
    t.integer  "endorser_user_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "name",             limit: 255
    t.string   "phone",            limit: 255
    t.datetime "sent_at"
    t.string   "uuid",             limit: 36
  end

  add_index "endorsement_requests", ["endorser_user_id"], name: "index_endorsement_requests_on_endorser_user_id", using: :btree
  add_index "endorsement_requests", ["user_id", "endorser_user_id"], name: "index_endorsement_requests_on_user_id_and_endorser_user_id", using: :btree
  add_index "endorsement_requests", ["user_id"], name: "index_endorsement_requests_on_user_id", using: :btree
  add_index "endorsement_requests", ["uuid"], name: "index_endorsement_requests_on_uuid", using: :btree

  create_table "endorsements", force: :cascade do |t|
    t.integer  "from_user_id"
    t.integer  "to_user_id"
    t.text     "text"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "approved",               default: false
    t.integer  "endorsement_request_id"
  end

  add_index "endorsements", ["from_user_id", "to_user_id"], name: "index_endorsements_on_from_user_id_and_to_user_id", unique: true, using: :btree

  create_table "experiences", force: :cascade do |t|
    t.integer  "organization_id"
    t.integer  "user_id"
    t.integer  "title_id"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.text     "description"
    t.string   "location",        limit: 255
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.boolean  "employee"
    t.integer  "department_id"
    t.text     "properties"
    t.string   "type",            limit: 255
    t.boolean  "incomplete",                  default: false
  end

  add_index "experiences", ["ended_at"], name: "index_experiences_on_ended_at", order: {"ended_at"=>:desc}, using: :btree
  add_index "experiences", ["organization_id", "user_id"], name: "index_organization_users_on_organization_id_and_user_id", using: :btree
  add_index "experiences", ["started_at"], name: "index_experiences_on_started_at", order: {"started_at"=>:desc}, using: :btree
  add_index "experiences", ["title_id"], name: "index_experiences_on_title_id", using: :btree
  add_index "experiences", ["type"], name: "index_experiences_on_type", using: :btree
  add_index "experiences", ["user_id", "type", "ended_at"], name: "index_experiences_on_user_id_and_type_and_ended_at", using: :btree
  add_index "experiences", ["user_id"], name: "index_experiences_on_user_id", using: :btree

  create_table "external_posts", force: :cascade do |t|
    t.integer  "postable_id"
    t.string   "postable_type", limit: 255
    t.integer  "user_id"
    t.string   "type",          limit: 255
    t.string   "url",           limit: 255
    t.datetime "deleted_at"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.text     "details"
  end

  create_table "facebook_users", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name",              limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "uid",               limit: 255
    t.string   "avatar_url",        limit: 255
    t.text     "auth_token"
    t.integer  "blue_collar_score"
    t.string   "invited_by",        limit: 255
  end

  add_index "facebook_users", ["uid"], name: "index_facebook_users_on_uid", using: :btree
  add_index "facebook_users", ["user_id"], name: "index_facebook_users_on_user_id", using: :btree

  create_table "global_aliases", force: :cascade do |t|
    t.integer  "aliasable_id"
    t.string   "aliasable_type", limit: 255
    t.string   "name",           limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "global_aliases", ["aliasable_id", "aliasable_type"], name: "index_global_aliases_on_aliasable_id_and_aliasable_type", using: :btree

  create_table "global_slugs", force: :cascade do |t|
    t.integer  "sluggable_id"
    t.string   "sluggable_type", limit: 255
    t.string   "slug",           limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "created_by_id"
  end

  add_index "global_slugs", ["slug"], name: "index_global_slugs_on_slug", using: :btree
  add_index "global_slugs", ["sluggable_type", "sluggable_id"], name: "by_sluggable", using: :btree

  create_table "image_dimensions", force: :cascade do |t|
    t.integer  "width"
    t.integer  "height"
    t.integer  "image_id"
    t.string   "type",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "image_dimensions", ["image_id"], name: "index_image_dimensions_on_image_id", using: :btree

  create_table "images", force: :cascade do |t|
    t.string   "file_url",           limit: 255
    t.string   "title",              limit: 255
    t.text     "description"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "photo_file_name",    limit: 255
    t.string   "photo_content_type", limit: 255
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "uuid",               limit: 36,  null: false
    t.integer  "project_id"
    t.string   "inheritance_type",   limit: 255
    t.boolean  "photo_processing"
    t.integer  "imageable_id"
    t.string   "imageable_type",     limit: 255
  end

  add_index "images", ["created_at"], name: "index_images_on_created_at", using: :btree
  add_index "images", ["imageable_type", "imageable_id"], name: "by_imageable", using: :btree
  add_index "images", ["inheritance_type"], name: "index_images_on_inheritance_type", using: :btree
  add_index "images", ["project_id"], name: "index_images_on_project_id", using: :btree
  add_index "images", ["uuid"], name: "index_project_images_on_uuid", using: :btree

  create_table "influencables", force: :cascade do |t|
    t.text     "title"
    t.string   "destination",     limit: 255
    t.integer  "displayed_count"
    t.datetime "first_displayed"
    t.datetime "last_displayed"
    t.datetime "declined_at"
    t.datetime "completed_at"
    t.integer  "user_id"
    t.string   "key",             limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "notifiable_id"
    t.string   "notifiable_type", limit: 255
    t.text     "description"
    t.string   "action_text",     limit: 255
  end

  add_index "influencables", ["notifiable_type", "notifiable_id"], name: "index_influencables_on_notifiable_type_and_notifiable_id", using: :btree
  add_index "influencables", ["user_id"], name: "index_influencables_on_user_id", using: :btree

  create_table "invitations", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "reason_id"
    t.string   "channel",      limit: 255
    t.datetime "sent_at"
    t.datetime "read_at"
    t.datetime "responded_at"
    t.string   "response",     limit: 255
    t.string   "uuid",         limit: 255
    t.string   "type",         limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "recipient_id"
    t.string   "reason_type",  limit: 255
    t.text     "salesforce"
    t.datetime "reminded_at"
  end

  add_index "invitations", ["reason_id"], name: "index_membership_invitations_on_membership_id", using: :btree
  add_index "invitations", ["uuid"], name: "index_membership_invitations_on_uuid", using: :btree

  create_table "invite_codes", force: :cascade do |t|
    t.string   "text",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "invitees", force: :cascade do |t|
    t.string   "email",         limit: 255
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "invite_code",   limit: 255
    t.boolean  "notifications",             default: true
    t.integer  "user_id"
    t.boolean  "send_invite",               default: false
    t.datetime "invited_at"
  end

  add_index "invitees", ["user_id"], name: "index_invitees_on_user_id", using: :btree

  create_table "job_applications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "job_post_id"
    t.string   "subject_line",        limit: 255
    t.text     "cover_letter"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "resume_file_name",    limit: 255
    t.string   "resume_content_type", limit: 255
    t.integer  "resume_file_size"
    t.datetime "resume_updated_at"
    t.string   "status",              limit: 255
    t.integer  "assignee_id"
    t.datetime "marked_ready_at"
    t.text     "text_resume"
    t.text     "notes"
  end

  add_index "job_applications", ["job_post_id"], name: "index_job_applications_on_job_post_id", using: :btree
  add_index "job_applications", ["user_id"], name: "index_job_applications_on_user_id", using: :btree

  create_table "job_posts", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "company_id"
    t.string   "title",                limit: 255
    t.text     "description"
    t.string   "slug",                 limit: 255
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "external_link",        limit: 255
    t.integer  "zip_code_id"
    t.datetime "published_at"
    t.text     "summary"
    t.string   "source",               limit: 255
    t.string   "recruiter_email",      limit: 255
    t.datetime "tweeted_at"
    t.string   "tweet",                limit: 255
    t.string   "tweeter",              limit: 255
    t.boolean  "featured",                         default: false
    t.string   "keywords",             limit: 255
    t.boolean  "visible",                          default: true
    t.string   "compensation",         limit: 255
    t.text     "details"
    t.boolean  "military",                         default: false
    t.string   "alternate_email",      limit: 255
    t.integer  "plan_subscription_id"
    t.string   "stripe_id",            limit: 255
    t.datetime "paid_at"
    t.integer  "paid_amount"
    t.string   "external_id",          limit: 255
    t.text     "source_metadata"
  end

  add_index "job_posts", ["company_id"], name: "index_job_posts_on_company_id", using: :btree
  add_index "job_posts", ["external_link"], name: "index_job_posts_on_external_link", using: :btree
  add_index "job_posts", ["slug"], name: "index_job_posts_on_slug", using: :btree

  create_table "keywords", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.integer  "category_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "user_id"
    t.integer  "instances",               default: 0
    t.string   "controller",  limit: 255
    t.string   "slug",        limit: 255
  end

  add_index "keywords", ["category_id"], name: "index_keywords_on_category_id", using: :btree
  add_index "keywords", ["slug"], name: "index_keywords_on_slug", using: :btree
  add_index "keywords", ["user_id"], name: "index_keywords_on_user_id", using: :btree

  create_table "license_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "issuer_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "slug",       limit: 255
  end

  add_index "license_types", ["issuer_id"], name: "index_license_types_on_issuer_id", using: :btree
  add_index "license_types", ["slug"], name: "index_license_types_on_slug", using: :btree

  create_table "licenses", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "file_url",               limit: 255
    t.string   "name",                   limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.boolean  "verified",                           default: false
    t.boolean  "cant_verify",                        default: false
    t.date     "expires"
    t.string   "lic_no",                 limit: 255
    t.text     "description"
    t.string   "issued_by",              limit: 255
    t.integer  "issuer_id"
    t.integer  "license_type_id"
    t.boolean  "authorize_verification",             default: false
    t.boolean  "incomplete",                         default: false
  end

  add_index "licenses", ["issuer_id"], name: "index_licenses_on_issuer_id", using: :btree
  add_index "licenses", ["license_type_id"], name: "index_licenses_on_license_type_id", using: :btree
  add_index "licenses", ["user_id"], name: "index_licenses_on_user_id", using: :btree

  create_table "membership_versions", force: :cascade do |t|
    t.string   "item_type",           limit: 255, null: false
    t.integer  "item_id",                         null: false
    t.string   "event",               limit: 255, null: false
    t.string   "whodunnit",           limit: 255
    t.text     "object"
    t.datetime "created_at"
    t.integer  "user_id"
    t.integer  "membershipable_id"
    t.string   "membershipable_type", limit: 255
    t.integer  "parent_id"
  end

  add_index "membership_versions", ["item_type", "item_id"], name: "index_membership_versions_on_item_type_and_item_id", using: :btree
  add_index "membership_versions", ["membershipable_type", "membershipable_id"], name: "index_membership_versions_on_membershipable", using: :btree
  add_index "membership_versions", ["user_id"], name: "index_membership_versions_on_user_id", using: :btree

  create_table "memberships", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "membershipable_id"
    t.string   "membershipable_type",       limit: 255
    t.string   "role",                      limit: 255
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "apprenticeship_program_id"
    t.integer  "added_by_id"
    t.integer  "pilot_membership_id"
  end

  add_index "memberships", ["membershipable_id", "membershipable_type"], name: "index_memberships_on_membershipable_id_and_membershipable_type", using: :btree

  create_table "messaging_conversations", force: :cascade do |t|
    t.string   "type",                   limit: 255
    t.integer  "created_by_id"
    t.string   "subject",                limit: 255
    t.text     "settings"
    t.string   "uuid",                   limit: 255
    t.datetime "archived_at"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "legacy_conversation_id"
    t.integer  "originator_id"
    t.string   "originator_type",        limit: 255
  end

  add_index "messaging_conversations", ["created_by_id"], name: "index_messaging_conversations_on_created_by_id", using: :btree
  add_index "messaging_conversations", ["originator_id", "originator_type"], name: "index_conversations_on_originator_id_and_originator_type", using: :btree
  add_index "messaging_conversations", ["updated_at"], name: "index_messaging_conversations_on_updated_at", using: :btree

  create_table "messaging_messages", force: :cascade do |t|
    t.integer  "conversation_id"
    t.integer  "user_id"
    t.text     "content"
    t.string   "uuid",                   limit: 255
    t.datetime "scheduled_at"
    t.datetime "posted_at"
    t.datetime "archived_at"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "legacy_message_id"
    t.integer  "legacy_conversation_id"
  end

  add_index "messaging_messages", ["conversation_id"], name: "index_messaging_messages_on_conversation_id", using: :btree
  add_index "messaging_messages", ["posted_at"], name: "index_messaging_messages_on_posted_at", using: :btree

  create_table "messaging_participant_details", force: :cascade do |t|
    t.integer  "conversation_id"
    t.integer  "participant_id"
    t.string   "participant_type", limit: 255
    t.text     "settings"
    t.datetime "muted_at"
    t.datetime "read_at"
    t.datetime "deleted_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "messaging_participant_details", ["conversation_id", "participant_id", "participant_type"], name: "by_conversation_participant", using: :btree
  add_index "messaging_participant_details", ["conversation_id"], name: "index_messaging_participant_details_on_conversation_id", using: :btree
  add_index "messaging_participant_details", ["participant_type", "participant_id"], name: "by_participant", using: :btree

  create_table "organization_accounts", force: :cascade do |t|
    t.integer  "organization_id"
    t.integer  "verified_by_id"
    t.integer  "verifiable_by_id"
    t.date     "ended_at"
    t.string   "name",             limit: 255
    t.text     "description"
    t.string   "account_type",     limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.text     "salesforce"
  end

  add_index "organization_accounts", ["organization_id"], name: "index_organization_accounts_on_organization_id", using: :btree

  create_table "organization_job_posts", force: :cascade do |t|
    t.integer  "job_post_id"
    t.integer  "organization_id"
    t.integer  "created_by_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "organization_memberships", force: :cascade do |t|
    t.integer  "organization_id"
    t.string   "membershipable_type", limit: 255
    t.integer  "membershipable_id"
    t.integer  "parent_member_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "organization_memberships", ["membershipable_id", "membershipable_type"], name: "organization_membershipable", using: :btree
  add_index "organization_memberships", ["organization_id"], name: "index_organization_memberships_on_organization_id", using: :btree

  create_table "organizations", force: :cascade do |t|
    t.string   "name",                                 limit: 255
    t.text     "description"
    t.string   "type",                                 limit: 255
    t.integer  "parent_id"
    t.integer  "industry_id"
    t.string   "entity",                               limit: 255
    t.string   "approximate_size",                     limit: 255
    t.string   "url",                                  limit: 255
    t.string   "slug",                                 limit: 255
    t.datetime "founded_at"
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.integer  "created_by_id"
    t.string   "email",                                limit: 255
    t.string   "phone",                                limit: 255
    t.string   "abbreviation",                         limit: 255
    t.boolean  "visible",                                          default: true
    t.integer  "cleaned_by_id"
    t.date     "cleaned_at"
    t.string   "seeded_by_source",                     limit: 255
    t.datetime "locked_admins_at"
    t.integer  "memberships_count",                                default: 0,    null: false
    t.integer  "non_gray_non_admin_memberships_count",             default: 0,    null: false
    t.integer  "non_gray_admin_memberships_count",                 default: 0,    null: false
    t.integer  "non_gray_total_memberships_count",                 default: 0,    null: false
    t.text     "metadata"
    t.text     "details"
  end

  add_index "organizations", ["parent_id"], name: "index_organizations_on_parent_id", using: :btree
  add_index "organizations", ["seeded_by_source"], name: "index_organizations_on_seeded_by_source", using: :btree
  add_index "organizations", ["slug"], name: "index_organizations_on_slug", using: :btree

  create_table "partnerships", force: :cascade do |t|
    t.integer  "partner_id"
    t.string   "partner_type",             limit: 255
    t.integer  "organization_id"
    t.string   "type",                     limit: 255
    t.datetime "started_at"
    t.datetime "ended_at"
    t.integer  "created_by_id"
    t.integer  "verified_by_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.datetime "verified_at"
    t.integer  "accepted_invitation_id"
    t.string   "accepted_invitation_type", limit: 255
  end

  add_index "partnerships", ["organization_id"], name: "index_partnerships_on_organization_id", using: :btree
  add_index "partnerships", ["partner_type", "partner_id"], name: "index_partnerships_on_partner_type_and_partner_id", using: :btree

  create_table "personal_statuses", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "kind",       limit: 255
  end

  add_index "personal_statuses", ["user_id"], name: "index_personal_statuses_on_user_id", using: :btree

  create_table "pilot_memberships", force: :cascade do |t|
    t.integer  "membershipable_id"
    t.string   "membershipable_type",      limit: 255
    t.integer  "user_id"
    t.string   "type",                     limit: 255
    t.datetime "started_at"
    t.datetime "ended_at"
    t.integer  "created_by_id"
    t.integer  "verified_by_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.datetime "verified_at"
    t.integer  "accepted_invitation_id"
    t.string   "accepted_invitation_type", limit: 255
    t.integer  "parent_id"
    t.string   "role",                     limit: 255
    t.integer  "plan_subscription_id"
    t.integer  "legacy_membership_id"
    t.text     "salesforce"
    t.datetime "activated_at"
  end

  add_index "pilot_memberships", ["id", "type"], name: "index_pilot_memberships_on_id_and_type", using: :btree
  add_index "pilot_memberships", ["membershipable_type", "membershipable_id"], name: "by_membershipable", using: :btree
  add_index "pilot_memberships", ["parent_id"], name: "index_pilot_memberships_on_parent_id", using: :btree
  add_index "pilot_memberships", ["user_id", "membershipable_type"], name: "index_pilot_memberships_on_user_id_and_membershipable_type", using: :btree
  add_index "pilot_memberships", ["user_id"], name: "index_pilot_memberships_on_user_id", using: :btree

  create_table "plan_subscriptions", force: :cascade do |t|
    t.integer  "plan_id"
    t.integer  "user_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.text     "metadata_snapshot"
    t.datetime "last_snapshot_at"
    t.datetime "verified_at"
    t.integer  "verified_by_id"
    t.datetime "activated_at"
    t.string   "stripe_id",         limit: 255
    t.datetime "renew_at"
    t.string   "coupon",            limit: 255
    t.text     "salesforce"
  end

  create_table "plans", force: :cascade do |t|
    t.string   "type",         limit: 255
    t.string   "name",         limit: 255
    t.text     "description"
    t.integer  "amount"
    t.string   "interval",     limit: 255
    t.datetime "published_at"
    t.text     "metadata"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "slug",         limit: 255
  end

  add_index "plans", ["type"], name: "index_plans_on_type", using: :btree

  create_table "positions", force: :cascade do |t|
    t.integer  "company_id"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "title",       limit: 255
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "title_id"
    t.datetime "migrated_at"
    t.integer  "new_id"
    t.string   "new_type",    limit: 255
  end

  add_index "positions", ["company_id"], name: "index_positions_on_company_id", using: :btree
  add_index "positions", ["user_id"], name: "index_positions_on_user_id", using: :btree

  create_table "project_memberships", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "project_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "uuid",       limit: 255
  end

  add_index "project_memberships", ["project_id"], name: "index_project_memberships_on_project_id", using: :btree
  add_index "project_memberships", ["user_id"], name: "index_project_memberships_on_user_id", using: :btree
  add_index "project_memberships", ["uuid"], name: "index_project_memberships_on_uuid", using: :btree

  create_table "projects", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title",        limit: 255
    t.text     "description"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "uuid",         limit: 255
    t.boolean  "visible",                  default: true
    t.integer  "images_count",             default: 0,    null: false
  end

  add_index "projects", ["user_id"], name: "index_projects_on_user_id", using: :btree
  add_index "projects", ["uuid"], name: "index_projects_on_uuid", using: :btree

  create_table "received_emails", force: :cascade do |t|
    t.string   "to",                limit: 255
    t.string   "from",              limit: 255
    t.string   "subject",           limit: 255
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.text     "body"
    t.integer  "organization_id"
    t.integer  "department_id"
    t.string   "type",              limit: 255
    t.integer  "created_by_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "reps", force: :cascade do |t|
    t.integer  "repable_id"
    t.string   "repable_type", limit: 255
    t.integer  "value",                    default: 1
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "user_id"
  end

  add_index "reps", ["repable_id", "repable_type"], name: "index_reps_on_repable_id_and_repable_type", using: :btree
  add_index "reps", ["user_id"], name: "index_reps_on_user_id", using: :btree

  create_table "shortened_urls", force: :cascade do |t|
    t.string   "slug",       limit: 255
    t.text     "params"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "shortened_urls", ["slug"], name: "index_shortened_urls_on_slug", using: :btree

  create_table "states", force: :cascade do |t|
    t.string "full_state", limit: 255
    t.string "name",       limit: 255
    t.string "slug",       limit: 255
  end

  create_table "system_emails", force: :cascade do |t|
    t.string   "key",        limit: 255
    t.integer  "user_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "subject",    limit: 255
  end

  create_table "system_text_messages", force: :cascade do |t|
    t.string   "key",        limit: 255
    t.integer  "user_id"
    t.text     "body"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "tests", force: :cascade do |t|
    t.string   "thing"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "titles", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.boolean  "visible",                       default: false
    t.string   "organization_type", limit: 255
    t.integer  "created_by_id"
    t.string   "slug",              limit: 255
  end

  add_index "titles", ["slug"], name: "index_titles_on_slug", using: :btree

  create_table "tools", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.string   "description",         limit: 255
    t.string   "url",                 limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.boolean  "visible",                         default: false
    t.integer  "user_tools_count",                default: 0,     null: false
    t.string   "slug",                limit: 255
  end

  add_index "tools", ["slug"], name: "index_tools_on_slug", using: :btree

  create_table "user_categories", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "verified",    default: true
  end

  add_index "user_categories", ["category_id"], name: "index_user_categories_on_category_id", using: :btree
  add_index "user_categories", ["user_id"], name: "index_user_categories_on_user_id", using: :btree

  create_table "user_connections", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "connection_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "type",          limit: 255
    t.string   "source",        limit: 255
    t.integer  "affinity",                  default: 0
    t.boolean  "visible",                   default: true
  end

  add_index "user_connections", ["connection_id", "type"], name: "index_user_connections_on_connection_id_and_type", using: :btree
  add_index "user_connections", ["connection_id", "user_id", "type", "source"], name: "user_to_user_by_type_and_source", unique: true, using: :btree
  add_index "user_connections", ["connection_id"], name: "index_user_connections_on_connection_id", using: :btree
  add_index "user_connections", ["type", "connection_id"], name: "index_user_connections_on_type_and_connection_id", using: :btree
  add_index "user_connections", ["type", "user_id"], name: "index_user_connections_on_type_and_user_id", using: :btree
  add_index "user_connections", ["user_id", "connection_id"], name: "index_user_connections_on_user_id_and_connection_id", using: :btree
  add_index "user_connections", ["user_id", "type"], name: "index_user_connections_on_user_id_and_type", using: :btree
  add_index "user_connections", ["user_id"], name: "index_user_connections_on_user_id", using: :btree

  create_table "user_invitations", force: :cascade do |t|
    t.string   "email",            limit: 255
    t.integer  "user_id"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "type",             limit: 255
    t.integer  "facebook_user_id"
    t.string   "phone",            limit: 255
    t.string   "name",             limit: 255
    t.boolean  "notification",                 default: true
    t.integer  "invited_id"
    t.string   "uuid",             limit: 36
    t.datetime "sent_at"
  end

  add_index "user_invitations", ["facebook_user_id"], name: "index_user_invitees_on_facebook_user_id", using: :btree
  add_index "user_invitations", ["invited_id"], name: "index_user_invitations_on_invited_id", using: :btree
  add_index "user_invitations", ["user_id"], name: "index_user_invitees_on_user_id", using: :btree
  add_index "user_invitations", ["uuid"], name: "index_user_invitations_on_uuid", using: :btree

  create_table "user_notification_subscriptions", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "subscribed",                default: "true"
    t.string   "notifier_type", limit: 255
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "user_notification_subscriptions", ["notifier_type"], name: "index_user_email_subscriptions_on_notifier_type", using: :btree
  add_index "user_notification_subscriptions", ["user_id"], name: "index_user_email_subscriptions_on_user_id", using: :btree

  create_table "user_tools", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "tool_id"
    t.boolean  "own"
    t.boolean  "can_use"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_tools", ["tool_id"], name: "index_user_tools_on_tool_id", using: :btree
  add_index "user_tools", ["user_id"], name: "index_user_tools_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                limit: 255
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "invite_code",          limit: 255
    t.string   "name",                 limit: 255
    t.string   "phone",                limit: 255
    t.integer  "zip_code_id"
    t.string   "website",              limit: 255
    t.string   "slug",                 limit: 255
    t.boolean  "admin",                            default: false
    t.boolean  "newsletter_subscribe",             default: false
    t.string   "authentication_token", limit: 255
    t.string   "first_name",           limit: 255
    t.string   "last_name",            limit: 255
    t.decimal  "years_experience"
    t.boolean  "visible",                          default: true
    t.text     "settings"
    t.string   "role",                 limit: 255
    t.datetime "date_of_birth"
    t.string   "screen_name",          limit: 255
    t.datetime "first_used_pilot_at"
    t.string   "phone_extension",      limit: 255
    t.string   "stripe_id",            limit: 255
    t.datetime "secure_activated_at"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", using: :btree
  add_index "users", ["zip_code_id"], name: "index_users_on_zip_code_id", using: :btree

  create_table "weekly_signup_summaries", force: :cascade do |t|
    t.date     "week"
    t.integer  "total_users"
    t.integer  "total_signups"
    t.integer  "total_android"
    t.integer  "total_iphone"
    t.integer  "total_facebook"
    t.text     "invite_code_counts"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "weekly_signup_summaries", ["week"], name: "index_weekly_signup_summaries_on_week", using: :btree

  create_table "worker_titles", force: :cascade do |t|
    t.integer  "title_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "zip_codes", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.string   "city_name",  limit: 255
    t.string   "state_name", limit: 255
    t.decimal  "latitude",               precision: 15, scale: 10
    t.decimal  "longitude",              precision: 15, scale: 10
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "full_state", limit: 255
    t.integer  "state_id"
    t.string   "slug",       limit: 255
    t.integer  "city_id"
  end

  add_index "zip_codes", ["city_id"], name: "index_zip_codes_on_city_id", using: :btree
  add_index "zip_codes", ["code"], name: "index_zip_codes_on_code", using: :btree
  add_index "zip_codes", ["state_id"], name: "index_zip_codes_on_state_id", using: :btree

end
